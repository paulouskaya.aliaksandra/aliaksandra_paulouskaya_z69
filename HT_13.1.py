from peewee import *

db = MySQLDatabase("test",
    user="root",
    password="YourHarbor123",
    host="localhost"
    )


class BaseModel(Model):
    class Meta:
        database = db


class table_1(BaseModel):
    Part_ID = PrimaryKeyField()
    Part = CharField(20)
    Cat = IntegerField()


table_1.create_table()


data_table_1 = [
    (1, "Квартиры", 505),
    (2, "Автомашины", 205),
    (3, "Доски", 10),
    (4, "Шкафы", 30),
    (5, "Книги", 160)]


# table_1.insert_many(data_table_1, fields=[
#     table_1.Part_ID, 
#     table_1.Part, 
#     table_1.Cat]).execute()


class table_2(BaseModel):
    Catnumb = PrimaryKeyField()
    Cat_name = CharField(20)
    Price = IntegerField()


table_2.create_table()


data_table_2 = [
    (10, "Стройматериалы", 105),
    (505, "Недвижимость", 210),
    (205, "Транспорт", 160),
    (30, "Мебель", 77),
    (45, "Техника", 65)]


# table_2.insert_many(data_table_1, fields=[
#     table_2.Catnumb, 
#     table_2.Cat_name, 
#     table_2.Price]).execute()



query = (table_1.select().join(table_2, on = (table_1.Cat == table_2.Catnumb)))


for i in query:
    print(i.Part, i.Cat_name, i.Price)



print("Done")

